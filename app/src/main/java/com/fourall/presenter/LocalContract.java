package com.fourall.presenter;

import com.fourall.domain.model.Local;

public interface LocalContract {

    interface View {
        void setUpLocal(Local local);

        void showLoadingLayout();

        void showErrorLayout();

        void showSuccessLayout();

        void showEmptyLayout();
    }

    interface Presenter {
        Local onSaveInstanceState();

        void onLoadInstanceState(Local aggregation);

        void loadLocal(final String idLocal);

        void refreshUi();

        void setView(LocalContract.View view);
    }
}

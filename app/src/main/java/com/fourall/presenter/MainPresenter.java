package com.fourall.presenter;


import com.fourall.domain.model.Lista;
import com.fourall.domain.service.ListaService;

public class MainPresenter implements MainContract.Presenter {
    private MainContract.View view;
    private ListaService service;
    private Lista localAggregation;

    public MainPresenter(ListaService service) {
        this.service = service;
    }

    @Override
    public Lista onSaveInstanceState() {
        return localAggregation;
    }

    @Override
    public void onLoadInstanceState(Lista aggregation) {
        this.localAggregation = aggregation;
    }

    @Override
    public void loadLista() {
        view.showLoadingLayout();
        service.getLocals().subscribe(aggregations -> {
            this.localAggregation = aggregations;
            refreshUi();
        }, throwable -> {
            view.showErrorLayout();
            throwable.printStackTrace();
        });
    }

    @Override
    public void refreshUi() {
        if (localAggregation != null) {
            view.showSuccessLayout();
            view.setUpLista(localAggregation);
        } else {
            view.showEmptyLayout();
        }
    }

    @Override
    public void retryLista() {
        loadLista();
    }

    @Override
    public void setView(MainContract.View view) {
        this.view = view;
    }
}

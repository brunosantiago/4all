package com.fourall.presenter;


import com.fourall.domain.model.Local;
import com.fourall.domain.service.LocalService;

public class LocalPresenter implements LocalContract.Presenter {
    private LocalContract.View view;
    private LocalService service;
    private Local localAggregation;

    public LocalPresenter(LocalService service) {
        this.service = service;
    }

    @Override
    public Local onSaveInstanceState() {
        return localAggregation;
    }

    @Override
    public void onLoadInstanceState(Local aggregation) {
        this.localAggregation = aggregation;
    }

    @Override
    public void loadLocal(final String idLocal) {
        view.showLoadingLayout();
        service.getLocal(idLocal).subscribe(aggregations -> {
            this.localAggregation = aggregations;
            refreshUi();
        }, throwable -> {
            view.showErrorLayout();
            throwable.printStackTrace();
        });
    }

    @Override
    public void refreshUi() {
        if (localAggregation != null) {
            view.showSuccessLayout();
            view.setUpLocal(localAggregation);
        } else {
            view.showEmptyLayout();
        }
    }

    @Override
    public void setView(LocalContract.View view) {
        this.view = view;
    }
}

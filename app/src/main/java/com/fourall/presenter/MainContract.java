package com.fourall.presenter;

import com.fourall.domain.model.Lista;
import com.fourall.domain.model.Local;

import java.util.List;

public interface MainContract {

    interface View {
        void setUpLista(Lista lista);

        void showLoadingLayout();

        void showErrorLayout();

        void showSuccessLayout();

        void showEmptyLayout();
    }

    interface Presenter {
        Lista onSaveInstanceState();

        void onLoadInstanceState(Lista aggregation);

        void loadLista();

        void refreshUi();

        void retryLista();

        void setView(MainContract.View view);
    }
}

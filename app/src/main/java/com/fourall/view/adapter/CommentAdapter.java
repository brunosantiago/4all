package com.fourall.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fourall.R;
import com.fourall.domain.model.Comments;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.AldermanViewHolder> {

    private List<Comments> mComment = new ArrayList<>();
    private Context context;

    public CommentAdapter(Context context, List<Comments> lista) {
        this.mComment = lista;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(AldermanViewHolder holder, int position) {
        Comments comment = mComment.get(position);

        if (comment != null) {
            holder.tvName.setText(comment.nome);
            holder.tvTitleComment.setText(comment.titulo);
            holder.tvComment.setText(comment.comentario);
            holder.ratingBar.setRating(comment.nota.intValue());
            loadImage(comment.urlFoto, holder.ivPhoto);
        }
    }

    @Override
    public AldermanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        AldermanViewHolder viewHolder = new AldermanViewHolder(view);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return mComment.size();
    }

    public class AldermanViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvTitleComment)
        TextView tvTitleComment;
        @BindView(R.id.tvComment)
        TextView tvComment;
        @BindView(R.id.ratingBar)
        RatingBar ratingBar;
        @BindView(R.id.ivPhoto)
        ImageView ivPhoto;

        public AldermanViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    protected void loadImage(String imgUrl, ImageView ivProfile) {
        if (imgUrl != null && !imgUrl.isEmpty()) {
            Glide.with(context)
                    .load(imgUrl)
                    .crossFade()
                    .into(ivProfile);
        } else {
            Glide.with(context)
                    .load(R.drawable.logo_4all_cinza)
                    .crossFade()
                    .into(ivProfile);
        }
    }
}

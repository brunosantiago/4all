package com.fourall.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fourall.R;
import com.fourall.view.LocalDetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListaAdapter extends RecyclerView.Adapter<ListaAdapter.AldermanViewHolder> {

    private List<String> mLista = new ArrayList<>();
    private Context context;

    public ListaAdapter(Context context, List<String> lista) {
        this.mLista = lista;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(AldermanViewHolder holder, int position) {
        String lista = mLista.get(position);

        if (lista != null) {
            holder.tvId.setText(lista);
            holder.ivPhoto.setImageResource(R.drawable.logo_4all_cinza);
        }
    }

    @Override
    public AldermanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        AldermanViewHolder viewHolder = new AldermanViewHolder(view);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }

    public class AldermanViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvId)
        TextView tvId;
        @BindView(R.id.ivPhoto)
        ImageView ivPhoto;

        public AldermanViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            String lista = mLista.get(getAdapterPosition());
            Intent intent = new Intent(context, LocalDetailActivity.class);
            intent.putExtra("listaId", lista);
            context.startActivity(intent);

        }
    }

    protected void loadImage(String imgUrl, ImageView ivProfile) {
        if (imgUrl != null && !imgUrl.isEmpty()) {
            Glide.with(context)
                    .load(imgUrl)
                    .crossFade()
                    .into(ivProfile);
        } else {
            Glide.with(context)
                    .load(R.drawable.logo_4all_cinza)
                    .crossFade()
                    .into(ivProfile);
        }
    }
}

package com.fourall.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.fourall.R;
import com.fourall.domain.model.Lista;
import com.fourall.domain.service.ListaService;
import com.fourall.presenter.MainContract;
import com.fourall.presenter.MainPresenter;
import com.fourall.view.adapter.ListaAdapter;
import com.fourall.view.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainContract.View {

    @BindView(R.id.recyclerview)
    RecyclerView authorList;

    @BindView(R.id.successContainer)
    LinearLayout successContainer;

    @BindView(R.id.errorContainer)
    LinearLayout errorContainer;

    @BindView(R.id.loadingContainer)
    LinearLayout loadingContainer;

    @BindView(R.id.emptyContainer)
    LinearLayout emptyContainer;

    MainContract.Presenter presenter;

    private ListaService listaService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        listaService = new ListaService();
        presenter = new MainPresenter(listaService);

        initialize();

    }

    private void initialize() {
        presenter.setView(this);
        presenter.loadLista();
    }

    @OnClick(R.id.btnRetry)
    void retryLocal() {
        presenter.retryLista();
    }

    @Override
    public void setUpLista(Lista lista) {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        ListaAdapter LocalListAdapter = new ListaAdapter(this, lista.id);
        authorList.setLayoutManager(manager);
        authorList.setAdapter(LocalListAdapter);
    }

    @Override
    public void showLoadingLayout() {
        successContainer.setVisibility(View.GONE);
        errorContainer.setVisibility(View.GONE);
        loadingContainer.setVisibility(View.VISIBLE);
        emptyContainer.setVisibility(View.GONE);
    }

    @Override
    public void showErrorLayout() {
        successContainer.setVisibility(View.GONE);
        errorContainer.setVisibility(View.VISIBLE);
        loadingContainer.setVisibility(View.GONE);
        emptyContainer.setVisibility(View.GONE);
    }

    @Override
    public void showSuccessLayout() {
        successContainer.setVisibility(View.VISIBLE);
        errorContainer.setVisibility(View.GONE);
        loadingContainer.setVisibility(View.GONE);
        emptyContainer.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyLayout() {
        successContainer.setVisibility(View.GONE);
        errorContainer.setVisibility(View.GONE);
        loadingContainer.setVisibility(View.GONE);
        emptyContainer.setVisibility(View.VISIBLE);
    }
}

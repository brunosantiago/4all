package com.fourall.view;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fourall.R;
import com.fourall.domain.model.Local;
import com.fourall.domain.service.LocalService;
import com.fourall.presenter.LocalContract;
import com.fourall.presenter.LocalPresenter;
import com.fourall.util.PermissionUtils;
import com.fourall.view.adapter.CommentAdapter;
import com.fourall.view.base.BaseActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LocalDetailActivity extends BaseActivity implements LocalContract.View, OnMapReadyCallback {

    @BindView(R.id.btnCall)
    Button btnCall;
    @BindView(R.id.btnServices)
    Button btnServices;
    @BindView(R.id.btnAddress)
    Button btnAddress;
    @BindView(R.id.btnComments)
    Button btnComments;
    @BindView(R.id.btnFavorites)
    Button btnFavorites;
    @BindView(R.id.ivPhoto)
    ImageView ivPhoto;
    @BindView(R.id.tvText)
    TextView tvText;
    @BindView(R.id.tvTitulo)
    TextView tvTitulo;
    @BindView(R.id.tvMapAddress)
    TextView tvMapAddress;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.lnComment)
    LinearLayout lnComment;
    @BindView(R.id.nScroll)
    NestedScrollView nScroll;

    private MapFragment mapFragment;
    private Local local;


    LocalContract.Presenter presenter;
    private LocalService localService;

    /**
     * Cria a activity, e na recebe o id do Local via Intent.
     *
     * @param savedInstanceState salva o estado da activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_detail);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        String idLocal = getIntent().getStringExtra("listaId");
        ButterKnife.bind(this);

        localService = new LocalService();
        presenter = new LocalPresenter(localService);

        if (!TextUtils.isEmpty(idLocal)) {
            initialize(idLocal);
        }

        PermissionUtils.checkCall(this);

    }

    private void initialize(final String idLocal) {
        presenter.setView(this);
        presenter.loadLocal(idLocal);
    }

    /**
     * Exibe o nome do Local no título da tela
     *
     * @param local
     */
    protected void setToolbar(final Local local) {
        if (local != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setTitle(local.titulo);
            toolbar.setSubtitle(local.cidade + " - " + local.bairro);
        }
    }

    protected void parseLocal(final Local local) {
        this.local = local;
        tvText.setText(local.texto);
        tvTitulo.setText(local.titulo);
        tvMapAddress.setText(local.endereco);
        if (!TextUtils.isEmpty(local.urlFoto)) {
            Glide.with(getApplicationContext())
                    .load(local.urlFoto)
                    .crossFade()
                    .into(ivPhoto);
        }
        parseComments(local);
    }

    protected void parseComments(final Local local) {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        CommentAdapter adapter = new CommentAdapter(this, local.comments);
        recyclerview.setLayoutManager(manager);
        recyclerview.setAdapter(adapter);
    }

    @OnClick(R.id.btnComments)
    void showComment() {
        nScroll.getParent().requestChildFocus(nScroll,lnComment);
    }

    @OnClick(R.id.btnServices)
    void startServices() {
        final Intent intent = new Intent(this, ServicosActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.btnAddress)
    void showAddress() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Endereço");
        alertBuilder.setMessage(local.endereco + " - "+ local.bairro + " - " + local.cidade);
        alertBuilder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
        });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    @OnClick(R.id.btnCall)
    void call() {
        String uri = "tel:"+local.telefone;

        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uri));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void setUpLocal(Local local) {
        setToolbar(local);
        parseLocal(local);
        setUpMapIfNeeded();
    }

    @Override
    public void showLoadingLayout() {

    }

    @Override
    public void showErrorLayout() {

    }

    @Override
    public void showSuccessLayout() {

    }

    @Override
    public void showEmptyLayout() {

    }

    private void setUpMapIfNeeded() {
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    protected void setUpMap(final GoogleMap map, final Local local) {

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(local.latitude, local.longitude));
        markerOptions.anchor(0.0f, 0.01f);

        map.addMarker(markerOptions);

    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        map.setIndoorEnabled(false);
        map.setBuildingsEnabled(false);
        map.getUiSettings().setZoomControlsEnabled(false);


        LatLng latLng = new LatLng(local.latitude, local.longitude);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        map.animateCamera(cameraUpdate);
        setUpMap(map, local);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}

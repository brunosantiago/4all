package com.fourall.domain.api;

import com.fourall.domain.model.Lista;
import com.fourall.domain.model.Local;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface ILocal {
    @GET("tarefa")
    Observable<Lista> findLocals();

    @GET("tarefa/{id}")
    Observable<Local> findLocalDetail(@Path(value = "id", encoded = true) final String id);
}

package com.fourall.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by bruno.santiago on 08/09/2016.
 */
public class Comments implements Parcelable {

    public String urlFoto;
    public String nome;
    public Double nota;
    public String titulo;
    public String comentario;

    protected Comments(Parcel in) {
        urlFoto = in.readString();
        nome = in.readString();
        nota = in.readDouble();
        titulo = in.readString();
        comentario = in.readString();
    }

    public static final Creator<Comments> CREATOR = new Creator<Comments>() {
        @Override
        public Comments createFromParcel(Parcel in) {
            return new Comments(in);
        }

        @Override
        public Comments[] newArray(int size) {
            return new Comments[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(urlFoto);
        parcel.writeString(nome);
        parcel.writeString(titulo);
        parcel.writeDouble(nota);
        parcel.writeString(comentario);
    }
}

package com.fourall.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Local implements Parcelable {
    public String id;
    public String cidade;
    public String bairro;
    public String urlFoto;
    public String urlLogo;
    public String titulo;
    public String telefone;
    public String texto;
    public String endereco;
    public double latitude;
    public double longitude;
    @SerializedName("comentarios")
    public ArrayList<Comments> comments;

    protected Local(Parcel in) {
        this.id = in.readString();
        this.cidade = in.readString();
        this.bairro = in.readString();
        this.urlFoto = in.readString();
        this.urlLogo = in.readString();
        this.titulo = in.readString();
        this.telefone = in.readString();
        this.texto = in.readString();
        this.endereco = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.comments = in.readArrayList(Comments.class.getClassLoader());
    }

    public static final Creator<Local> CREATOR = new Creator<Local>() {
        @Override
        public Local createFromParcel(Parcel in) {
            return new Local(in);
        }

        @Override
        public Local[] newArray(int size) {
            return new Local[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.cidade);
        dest.writeString(this.bairro);
        dest.writeString(this.urlFoto);
        dest.writeString(this.urlLogo);
        dest.writeString(this.titulo);
        dest.writeString(this.telefone);
        dest.writeString(this.texto);
        dest.writeString(this.endereco);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeList(this.comments);
    }
}

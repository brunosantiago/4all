package com.fourall.domain.service;

import com.fourall.domain.api.FourCallApi;
import com.fourall.domain.api.ILocal;
import com.fourall.domain.model.Lista;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ListaService implements Observable.Transformer<Lista,Lista> {

    private ILocal iTemis;

    public ListaService() {
        this.iTemis = FourCallApi.getRetrofit().create(ILocal.class);
    }

    public Observable<Lista> getLocals() {
        return iTemis.findLocals().compose(this);
    }

    @Override
    public Observable<Lista> call(Observable<Lista> tObservable) {
        return tObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}

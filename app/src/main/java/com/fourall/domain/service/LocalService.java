package com.fourall.domain.service;

import com.fourall.domain.api.FourCallApi;
import com.fourall.domain.api.ILocal;
import com.fourall.domain.model.Local;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LocalService implements Observable.Transformer<Local, Local> {

    private ILocal iTemis;

    public LocalService() {
        this.iTemis = FourCallApi.getRetrofit().create(ILocal.class);
    }

    public Observable<Local> getLocal(final String idLocal) {
        return iTemis.findLocalDetail(idLocal).compose(this);
    }

    @Override
    public Observable<Local> call(Observable<Local> tObservable) {
        return tObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
